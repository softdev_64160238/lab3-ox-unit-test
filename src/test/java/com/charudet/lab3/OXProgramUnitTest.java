/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.charudet.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class OXProgramUnitTest {
    
    public OXProgramUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoPlayBy_O_output_false(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "O";
        assertEquals(false, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckWinRow2By_O_output_true(){
        String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckWinRow1By_X_output_true(){
        String[][] table = {{"X","X","X"},{"-","-","-"},{"O","O","-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckWinRow2By_X_output_true(){
        String[][] table = {{"-","O","O"},{"X","X","X"},{"O","O","-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    
    
    @Test
    public void testCheckWinNoPlayBy_X_output_false(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "X";
        assertEquals(false, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckWinCol1By_O_output_true(){
        String[][] table = {{"O","X","X"},{"O","X","O"},{"O","-","-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckWinCol2By_O_output_true(){
        String[][] table = {{"X","O","-"},{"X","O","X"},{"-","O","-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckWinCol3By_X_output_true(){
        String[][] table = {{"-","O","X"},{"-","-","X"},{"X","O","X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    
    
    @Test
    public void testCheckwinDiagonal1By_X_output_true(){
        String[][] table = {{"X","O","X"},{"-","X","O"},{"-","O","X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckwinDiagonal2By_X_output_true(){
        String[][] table = {{"X","O","X"},{"O","X","O"},{"X","X","O"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckwinDiagonal1By_O_output_true(){
        String[][] table = {{"O","X","O"},{"-","O","X"},{"-","X","O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckwinDiagonal2By_O_output_true(){
        String[][] table = {{"X","O","O"},{"-","O","X"},{"O","X","X"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    
    
    @Test
    public void testCheckDraw_output_true(){
        String[][] table = {{"X","O","X"},{"X","O","O"},{"O","X","X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
}
